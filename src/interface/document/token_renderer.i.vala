public interface Dragonstone.Interface.Document.TokenRenderer : Object {
	
	public abstract void append_token(Dragonstone.Ui.Document.Token token);
	
	public abstract void reset_renderer();
}
