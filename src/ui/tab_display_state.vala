public enum Dragonstone.Ui.TabDisplayState {
	LOADING,
	ERROR,
	CONTENT,
	BLANK;
}
