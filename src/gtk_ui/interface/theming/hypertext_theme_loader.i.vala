public interface Dragonstone.GtkUi.Interface.Theming.HypertextThemeLoader : Object {
	
	public abstract Dragonstone.GtkUi.Interface.Theming.HypertextViewTheme? get_theme_by_name(string name);
	
}
